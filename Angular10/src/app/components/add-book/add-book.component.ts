import { Component, OnInit } from '@angular/core';
import { BookService } from "src/app/services/bookService";

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  book = {
    title: '',
    auteur: '',
    genre:'',
    prix: '',
    description: '',
    published: false
  };
  submitted = false;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
  }

  saveBook(): void {
    const data = {
      title: this.book.title,
      auteur: this.book.auteur,
      genre: this.book.genre,
      prix:  this.book.prix,
      description: this.book.description
    };

    this.bookService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newBook(): void {
    this.submitted = false;
    this.book = {
      title: '',
      auteur: '',
      genre:'',
      prix: '',
      description: '',
      published: false
    };
  }


}
