# Lancer l'application.

On doit d'abord installer les modules/packages dans chaque dossier. Run `npm install` -> dossier Angular10 et Nodejs.


## Development server

Run `ng serve --port 8081` dans le dossier Angular10. `http://localhost:8081/`  

Run `nodemon server.js` dans le dossier Nodejs. `http://localhost:8080/api/books`

## Mongodb

La base de donnée est dans un cluster sur le cloud. Toutes les informations pour se connecter se trouver dans le fichier `db.config.js`.
